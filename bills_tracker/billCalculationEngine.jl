# Author: Jaime Lopez-Merizalde
# Date: 08 10 2020
# Last Update: 08 10 2020
# Title: billCalculationEngine
# Descripition:
#  does all computation work necessary for bill_tracker_script
# this code written in Julia 0.5.4
# REPO:

#------Sums calculator
function monthlySumsCalculator(bill_data; roommates = 3)

    #skip the naming row
    #skip the month column
    bill_data_array = bill_data[2:end,2:end]

    #attach the month to the calculated data array
    return [bill_data[2:end,1] sum(bill_data_array,2) round(sum(bill_data_array,2)/roommates,3) ]

end


function serviceSumsCalculator(bill_data)

    #skip the naming row
    #skip the month column
    bill_data_array = bill_data[2:end,2:end]

    reshapedarray = reshape(bill_data[1,2:end],1,size(bill_data[1,2:end],1))

    #attach the service label
    return [reshapedarray;sum(bill_data_array,1)]

end
