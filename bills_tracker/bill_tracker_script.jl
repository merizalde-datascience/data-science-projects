# Author: Jaime Lopez-Merizalde
# Date: 08 10 2020
# Last Update: 08 10 2020
# Title: bill_tracker_script
# Descripition:
#  given bill data with date, water, electric and internet fields,
#   print out the roommate split along with historical data of previous month's bills data for comparison.
# this code written in Julia 0.5.4
# REPO:

##-----Libraries
include("billCalculationEngine.jl")

##DATA
##-----DATA
#-----data file names
#
data_bills_FN_pre = "bill_data_file.txt"
data_bills_FN = "$(pwd())/$data_bills_FN_pre"


save_results_FN = "$(pwd())/bills_results_$(now()).txt"

#-----data loading
data_bills = readdlm(data_bills_FN,',')
data_bills_size = size(data_bills,1)



#----- Call to engine calculations
roommates = 3
month_to_month_total = monthlySumsCalculator(data_bills;roommates = roommates)
service_to_service_total = serviceSumsCalculator(data_bills)

#----- Printout
println("*********************")
println("Showing Monthly Bills Data from: $(data_bills[2,1]) to $(data_bills[end,1])")
println("*********************")
println("This Month Split with $roommates Roommates:")
println("Month $(data_bills[1,2]) $(data_bills[1,3]) $(data_bills[1,4])  Total \ Split")
println("$(month_to_month_total[end,1])  \ $(data_bills[end,2])\ \ $(data_bills[end,3]) \ \ $(data_bills[end,4]) \ \ \ \  $(month_to_month_total[end,2]) $(month_to_month_total[end,3])")
println("*********************")
println("Bills paid in total to date:")
println("$(service_to_service_total[1,1]): $(service_to_service_total[2,1])")
println("$(service_to_service_total[1,2]): $(service_to_service_total[2,2])")
println("$(service_to_service_total[1,3]): $(service_to_service_total[2,3])")
println("*********************")
println("Month-to-month totals: ")
println("month total total / roommates")
for idx_month_to_month = 1:size(data_bills,1)-1
    println("$(month_to_month_total[idx_month_to_month,1]) $(month_to_month_total[idx_month_to_month,2]) $(month_to_month_total[idx_month_to_month,3])")
end

#-----TO SAVE
# #-----Save raw student data to a .txt or csv file
# open("$save_results_FN", "w") do io
#     writedlm(io, results_data,',')
# end
