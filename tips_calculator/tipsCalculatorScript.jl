# Author: Jaime Lopez-Merizalde
# Date: 29 10 2020
# Last Update: 1 11 2020
# Title: tipsCalculatorScript.jl
# Descripition:
#  Calculate Grade and save to a txt file
# this code written in Julia 0.5.4
# REPO:
#NOTES:written for Julia 0.5.4
#

##-----Libraries
# include("$(pwd())/library/engines/names_splitting_engine.jl")
# include("$(pwd())/library/engines/subStringToString.jl")
# include("$(pwd())/library/engines/addStudentGradesEngine.jl")

using CSV

##DATA
##-----DATA
#-----data file names
#

#load
data_csv_raw_FN_pre = "credit_card_bill_data.txt"
data_csv_raw_FN = "$(pwd())/$data_csv_raw_FN_pre"

data_cash_payments_pre = "cash_bill_data.txt"
data_cash_payments = "$(pwd())/$data_cash_payments_pre"

#save
save_tips_report_FN = "tips_report.txt"
save_tips_report = "$(pwd())/$save_tips_report_FN"

save_tips_report_formatted_FN = "tips_report_formatted.txt"
save_tips_report_formatted = "$(pwd())/$save_tips_report_formatted_FN"

#-----data loading
data_credit_card_bill = readdlm(data_csv_raw_FN,',')
data_cash_bill = readdlm(data_cash_payments, ',')

#----Do Some Checks
#This is Empty

#----Some Field names:
#includes headers
totalEntries = size(data_credit_card_bill,1)-1
totalCashEntries = size(data_cash_bill,1) - 1

totalTips = Float64(1)
totalCreditCardAmount = Float64(1)
TotalTotals = Float64(1)
PercentTips = Float64(1)

TipsReport = Array{Any}(2,9)

#----Initialize  Calculation Arrays
creditCardAmount = data_credit_card_bill[2:end,2]
cashAmount = data_cash_bill[2:end,2]

tenderedCashAmount = data_cash_bill[2:end,3]

tipsAmountCredit = data_credit_card_bill[2:end,3]
tipsAmountCash = tenderedCashAmount - cashAmount

TotalsCredit = data_credit_card_bill[2:end,4]

PercentTipsCreditArray = Array{Float64}(totalEntries,1)
PercentTipsCreditArray = tipsAmountCredit./creditCardAmount

#----Do Some Work
totalCreditCardAmount = round(sum(creditCardAmount),4)
totalCashAmount = round(sum(cashAmount),4)

totalTipsCreditOnly = round(sum(tipsAmountCredit),4)
totalTipsCashOnly = round(sum(tipsAmountCash),4)

totalTips = round(totalTipsCreditOnly+totalTipsCashOnly,4)
TotalTotals = round(sum(TotalsCredit), 4)

PercentTipsCreditOnly = round(totalTipsCreditOnly/totalCreditCardAmount*100, 4)
PercentTipsCashOnly = round(totalTipsCashOnly/totalCashAmount*100,4)
PercentTipsCreditAndCash = round(totalTips/(totalCreditCardAmount + totalCashAmount)*100 ,4)

TipsReport[1,:] = ["Total Credit Card Amount ", "Total Cash Amount ","Total Totals ", "Total Tips (Credit Only) ", "Total Tips (Cash Only) ", "Total Tips ", "Percent Tips (Credit Only) ", "Percent Tips (Cash Only) ", "Percent Tips Total "]


TipsReport[2,:] = [totalCreditCardAmount, totalCashAmount, TotalTotals,
totalTipsCreditOnly, totalTipsCashOnly, totalTips, PercentTipsCreditOnly, PercentTipsCashOnly,  PercentTipsCreditAndCash]
# TipsReport = tipsReportFormatter(TipsReport, max_Car_Count_Array);

function tipsReportFormatter(TipsReport);

    numTipCols = size(TipsReport,2)

    tipsHeader = TipsReport[1,:]
    tipsValues = TipsReport[2,:]

    FormattedTipsReport = formatArrayForDelimStyle(TipsReport)

    # tipsHeaderCharCount = charCountEngine(tipsHeader)
    # tipsValuesToStrings = convertDigitsToString(tipsValues)
    #
    # formatCharCountArray = Array{Int16}(numTipCols,1)
    # for idx = 1:size(tipsReportArray,1)
    #     formatCharCountArray[idx] = compareIntToString(tipsHeaderCharCount[idx], tipsValuesToStrings[idx])
    # end

    return FormattedTipsReport;
    end

function charCountEngine(arrayToCount)
    numFields = size(arrayToCount,1)

    charCountArray = Array{Int16}(numFields,1)

    for idx = 1:numFields
        charCountArray[idx] = length(arrayToCount[idx])
    end

    return charCountArray;
end

function convertDigitsToString(Array)

    sizeArray = size(Array,1)
    ConvertedArray = Array{Int16}(sizeArray)

    for idx_Array_Entry = 1:sizeArray
        ConvertedArray[idx_Array_Entry] = string(Array[idx_Array_Entry])
    end

    return ConvertedArray
    end

function compareIntToString(num1, string1; maxCharTolerance=15)

    lengthString1 = length(string1)

    return maximum(num1,lengthString1, maxCharTolerance)
    end

function createCharacterCountArray(field_array)
    field_array_size = size(field_array,1)

    #for each element in the array populate a new array with letter counts
    character_count_array = Array{Any}(field_array_size,2)
    #take field array place in first column
    character_count_array[:,1] = field_array
    for idx_fields = 1:field_array_size
        #count field array chars place in second column
        character_count_array[idx_fields,2]=length(field_array[idx_fields])
    end

    return character_count_array
    end

function formatArrayForDelimStyle(unformatted_array)

    unformatted_array_size = size(unformatted_array)

    formatted_array = Array{Any}(unformatted_array_size)

    character_count_array = createCharacterCountArray(unformatted_array[1,:])

    #take the fields row
    formatted_array[1,:] = unformatted_array[1,:]

    #check each element in the unformatted array
    for idx_data_rows = 2:unformatted_array_size[1]
        #start with second row
        for idx_data_columns = 1:unformatted_array_size[2]
            #go column by column

            unformatted_array_element = unformatted_array[idx_data_rows,idx_data_columns]

            character_count_array_element = character_count_array[idx_data_columns,2]

            unformatted_array_element_type = typeof(unformatted_array_element)

            unformatted_array_type = typeof(unformatted_array[idx_data_rows,idx_data_columns])

            #check data type first
            #work on strings first
            if unformatted_array_element_type==String || unformatted_array_element_type==SubString{String}
                #if it's a string we need to count the characters

                unformatted_array_element_count = length(unformatted_array_element)
                #element size too large
                if (unformatted_array_element_count>character_count_array_element)

                    #if the character count in unformatted array is LARGER then downsize for the formatted array
                    formatted_array[idx_data_rows,idx_data_columns] = string(unformatted_array_element[1:character_count_array_element])

                #same size
                elseif unformatted_array_element_count==character_count_array_element
                    formatted_array[idx_data_rows,idx_data_columns]=string(unformatted_array_element)

                #element size too small
                elseif (unformatted_array_element_count<character_count_array_element)

                    unformatted_array_element_count = length(unformatted_array_element)
                    char_deficiency = character_count_array_element - unformatted_array_element_count

                    #add spaces to make up for the deficiency
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element
                    for idx_spaces = 1:char_deficiency
                        formatted_array[idx_data_rows,idx_data_columns]=string(formatted_array[idx_data_rows,idx_data_columns]," ")
                    end
                end

            #next work on the actual numberical values
            #first change the number to a string
        elseif unformatted_array_element_type == Int16  || unformatted_array_element_type ==Int64 || unformatted_array_element_type== Float64
            #if it's a float, we need to convert to a character array and count that!

                #change numericals to strings
                unformatted_array_element_as_string = string(unformatted_array_element)
                unformatted_array_element_count = length(unformatted_array_element_as_string)

                #element size too large
                if (unformatted_array_element_count>character_count_array_element)

                    #if the character count in unformatted array is LARGER then downsize for the formatted array
                    formatted_array[idx_data_rows,idx_data_columns] = unformatted_array_element_as_string[1:character_count_array_element]

                #same size
                elseif unformatted_array_element_count==character_count_array_element
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element_as_string

                #element size too small
                elseif (unformatted_array_element_count<character_count_array_element)

                    char_deficiency = character_count_array_element - unformatted_array_element_count

                    #add spaces to make up for the deficiency
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element_as_string
                    for idx_spaces = 1:char_deficiency
                        spacedOutArrayElement = string(formatted_array[idx_data_rows,idx_data_columns]," ")
                        formatted_array[idx_data_rows,idx_data_columns]=spacedOutArrayElement

                    end
                end
            end

        end
    end
    return formatted_array, character_count_array
    end


(TipsReportFormatted,charCountArray) = tipsReportFormatter(TipsReport);
TipsReportAsColumn = Array{Any}(size(TipsReport)[2],size(TipsReport)[1])
TipsReportAsColumn[:,1] = TipsReport[1,:]
TipsReportAsColumn[:,2] = TipsReport[2,:]

#----Populate total Tips
#-----TO SAVE
#-----Save raw student data to a .txt or csv file
open("$save_tips_report_formatted", "w") do io
    writedlm(io, TipsReportAsColumn,',')
end

#-----Save raw student data to a .txt or csv file
open("$save_tips_report", "w") do io
    writedlm(io, TipsReport,',')
end
