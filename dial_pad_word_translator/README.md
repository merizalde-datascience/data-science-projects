Author: Jaime Lopez-Merizalde
Date:  29 9 2020 
Last Update: 7 9 2020 
Title: DIAL_PAD_WORD_TRANSLATOR 

**Description:**

The dial pad word translator takes in a number pressed on a standard 0-9 digit phone pad with English letters and finds all possible words made with that number in the given order.

This is encompassed as a single Julia .jl file with the following sections:

1. data: relevant data used in the dial pad operation. specifies which dictionary to use.
by default, this is the usa.txt dictionary of 61337.

2. user input: number to decode, which by default is 26337. This can be any number of any length.

3. dial pad: the dial pad definition which prescribes letters to key presses. This can be changed to different language settings. 

4. Key Press interpreter engine: takes the user input and interprets as array of possible letters from the dial pad

5. Word engine: several functions that decode and cross check the interpreted key press with the defined dictionary to find any words possible.

    5.a build all possible words with the word builder and 

    5.b cross check a possible word with the dictionary

    5.c cycle through all permutations of the interpreted key press

6. Word Search Script: defines the procedure of materials required for a successful search. initiates the index limitations, initiates the permuter tracker that stores all possible combinations of words from the interpreter, initiates a found word dictionary and populates this dictionary by using the word engine with the word tracker 

7. Reports the results with the words found and relevant data, such as which dictionary was used and how many words it cross-checked with.


**To Run**

Through Julia on: Version 0.5.2 or above.

1. download this repo.

2. in an IDE, enter the keypad press by updating the value 'NN'

3. in a terminal window, navigate to the repo loction 

4. execute the file in the terminal: include("dial_pad_word_translator.jl")






