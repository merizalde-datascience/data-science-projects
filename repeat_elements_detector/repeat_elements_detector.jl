# Author: Jaime Lopez-Merizalde
# Date:  27 9 2020
# Last Update: 7 9 2020
# Title: repeat_elements_translator
# Descripition:
#  given array of strings, return all elements that are repeated
# this code is written in Julia 0.5.4

#----DATA
#input the string
arr = ["ACEA", "ADVANCED", "CONTINUING", "EDUCATION", "ASSOCIATION", "ACEA", "ADVANCED"]

N1 = size(arr,1)

##repeat elements detector engine
idx_slct_array = zeros(N1)
for idx = 1:N1
    test_el = arr[idx]

    for idx_2 = idx+1:N1
        if test_el == arr[idx_2]
            idx_slct_array[idx_2]=1
        end
    end
end

N2 = Int(sum(idx_slct_array))

idx_3 = 1
array_not_unique = Array{Any}(N2,1)
for selector = 1:N1
    if idx_slct_array[selector] == 1
        array_unique[idx_3] = arr[selector]
        idx_3+=1
    end
end

#----Report elements found as unique
println("None Unique Elements \n")
println(array_not_unique)
