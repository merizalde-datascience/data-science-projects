# Author: Jaime Lopez-Merizalde
# Date: 13 10 2020
# Last Update: 28 11 2020
# Title: raw_grade_data_cleaer
# Descripition:
#  manually, or otherwise, populated data is cleaned to match the correct conventions for
#   canvas and other tools

# this code written in Julia 0.5.4
# REPO:
#NOTES:written in Julia 0.5.4
#
#


##-----Libraries
include("$(pwd())/library/engines/names_splitting_engine.jl")
include("$(pwd())/library/engines/name_search_engine.jl")
include("$(pwd())/library/engines/checkProblemsAnsweredEngine.jl")
include("$(pwd())/library/engines/subStringToString.jl")
# using DelimitedFiles

##DATA
##-----DATA
#-----data file names
#
#data to cross check
data_to_clean_FN_ = "midterm_data_3.txt"
data_to_clean_FN = "$(pwd())/$data_to_clean_FN_"

#cross check with this file as the baseline
data_Xcheck_reference_FN_ = "2020-11-28T1810_Grades-MATH-1230-05Fa20.csv"
data_Xcheck_reference_FN = "$(pwd())/$data_Xcheck_reference_FN_"

save_data_cleaned_FN_ = "$(data_to_clean_FN_[1:end-4])_cleaned$(data_to_clean_FN_[end-3:end])"
save_data_cleaned_FN = "$(pwd())/$save_data_cleaned_FN_"

data_key_FN_pre = "midterm_data_3.txt"
data_key_FN = "$(pwd())/$data_key_FN_pre"
#-----data file names

#-----data loading
data_to_clean = readdlm(data_to_clean_FN_,',')
data_to_clean_size = size(data_to_clean,1)
data_to_clean_size_2 = size(data_to_clean,2)
println("columns of data to clean: $data_to_clean_size_2")

data_to_clean_fields = data_to_clean[1,:]

data_Xcheck_reference = readdlm(data_Xcheck_reference_FN_,',')
data_Xcheck_reference_size = size(data_Xcheck_reference,1)
data_Xcheck_reference_fields = data_Xcheck_reference[1,:]

data_key = readdlm(data_key_FN,',')
data_key_size = size(data_key[2,:],1)
#-----data loading

## DATA CLEANER script:


#-----_Correct Names Engine

#split names: using only the names
#not formatted with a header by default
data_Xcheck_split_names = namesSplittingEngine(data_Xcheck_reference[3:end,1]; header = false)

#start off with the same data
data_cleaned = data_to_clean

header_row_correction = 1

for idx_name = header_row_correction+1:data_to_clean_size

    #set the last name baseline
    #format to capitals
    last_name_Xcheck = uppercase(data_Xcheck_split_names[idx_name-header_row_correction,1])

    first_name_Xcheck = uppercase(data_Xcheck_split_names[idx_name-header_row_correction,2])

    bool_name_found, idx_name_found = nameSearchEngine(last_name_Xcheck, first_name_Xcheck, data_to_clean[2:end,1:2])
    #if we found the name, nothing needs to be done!

    #if we don't find the name, we need to check the flipped version
    if !bool_name_found
        println("name: $last_name_Xcheck, $first_name_Xcheck not found on first pass!")

        #flip the first and last names
        last_name_Xcheck = uppercase(data_Xcheck_split_names[idx_name-header_row_correction,2][2:end])

        first_name_Xcheck = uppercase(data_Xcheck_split_names[idx_name-header_row_correction,1])

        bool_name_found, idx_name_found = nameSearchEngine(last_name_Xcheck, first_name_Xcheck, data_to_clean[2:end,1:2])

        #now reset to the appropriate name in the clean data
        if bool_name_found
            println("found on second pass!")
            println("with index: $(idx_name_found[1]) \n")
            data_cleaned[idx_name_found+1,1] = first_name_Xcheck
            data_cleaned[idx_name_found+1,2] = " $(last_name_Xcheck)"


        #we didn't find the name at all!
        else
            println("$first_name_Xcheck, $last_name_Xcheck not found!! on second pass after a name-flip!")
            println("SUGGEST PERSON MAY NOT HAVE SUBMITTED OR NAME IS NOT MATCHING AT ALL IN RAW CANVAS FILE")
            println("id in raw data to clean: $idx_name \n")
        end
    end
end

#-----_Correct Problems Answered Engine
#check if some people have too many answers, entered incorrectly
flagged_students = checkProblemsAnsweredEngine(data_cleaned, data_key_size)


#-----_Correct Problems Answered Engine

#-----TO DISPLAY
#-----FLAGGED students
if (size(collect(keys(flagged_students)),1)>0)
    println("*****************")
    println("Flagged Students. Check them out in the data")
    for key in keys(flagged_students)
        println(flagged_students[key])
    end
end


#-----TO SAVE
#-----Save raw student data to a .txt or csv file
open("$save_data_cleaned_FN", "w") do io
    writedlm(io, data_cleaned,',')
end
