# Author: Jaime Lopez-Merizalde
# Date: 11 10 2020
# Last Update: 28 11 2020
# Title: grade_csv_updater_for_canvas
# Descripition:
#  Calculate Grade and save to a txt file
# this code written in Julia 0.5.4
# REPO:
#NOTES:written for Julia 0.5.4


##-----Libraries
include("$(pwd())/library/engines/names_splitting_engine.jl")
include("$(pwd())/library/engines/subStringToString.jl")
include("$(pwd())/library/engines/addStudentGradesEngine.jl")

using CSV

##DATA
##-----DATA
#-----data file names
#

#load
data_csv_raw_FN_pre = "2020-11-28T1810_Grades-MATH-1230-05Fa20.csv"
data_csv_raw_FN = "$(pwd())/$data_csv_raw_FN_pre"

data_graded_midterm_FN_pre= "GRADED_MIDTERM_3_RAW.txt"
data_graded_midterm_FN = "$(pwd())/$data_graded_midterm_FN_pre"

#save
save_graded_csv_w_midterm_FN_pre = "$(data_csv_raw_FN_pre[1:end-4])_withMidterm3.csv"
save_graded_csv_w_midterm_FN = "$(pwd())/$save_graded_csv_w_midterm_FN_pre"


#-----data loading
data_raw_canvas = readdlm(data_csv_raw_FN,',')
# data_raw_canvas_size = size(data_raw_canvas)

data_graded_midterm = readdlm(data_graded_midterm_FN, ',')

#----Some Field names:
midterm_name = "Midterm 3"
exam_points = 90


#-----manipulate csv to add the student score to the CSV

data_raw_canvas_w_midterm = addStudentGradesEngine(data_raw_canvas, data_graded_midterm, midterm_name, exam_points)


#-----TO SAVE
#-----Save raw student data to a .txt or csv file
open("$save_graded_csv_w_midterm_FN", "w") do io
    writedlm(io, data_raw_canvas_w_midterm,',')
end
