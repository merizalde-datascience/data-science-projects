# Author: Jaime Lopez-Merizalde
# Date: 26 10 2020
# Last Update: 27 10 2020
# Title: checkProblemsAnsweredEngine.jl
# Descripition:
#  check if raw data has the correct number of answers provided and that they're in bounds!
# REPO:
#NOTES:written in Julia 0.5.4


#---- Library
include("$(pwd())/library/engines/subStringToString.jl")

function checkAnswerEndSpaceEngine(answer_vector, data_key_size)
    answer_length = size(answer_vector,1)
    final_character = subStringToString(answer_vector[end])

    if (final_character!=" " && data_key_size < answer_length)
        return false
    else
        return true
    end
end

function checkEachAnswerEngine(answer_vector, data_key_size)
    #notes: this assumes "" and " " are
    # invalid but also doesn't care if they are if it's final character
    possibleAnswers = [" A"," B"," C"," D"]
    answer_length = size(answer_vector,1)

    #check each entry first to see if it's valid
    charCounter = 1
    isValidAnswer = true
    while isValidAnswer && charCounter <= answer_length

        index_value = find(x->x==answer_vector[charCounter], possibleAnswers)

        if size(index_value,1)>=1
            charCounter += 1
        elseif size(index_value,1)==0
            #bad word if it's not one of the good chars before we check the last
            if charCounter < answer_length
                isValidAnswer = false
            else
                charCounter += 1
            end
        end

    end

    return isValidAnswer
end

function checkProblemsAnsweredEngine(data_cleaned, data_key_size)

    num_students = size(data_cleaned,1)-1
    starter_row = 2

    flagged_student_counter = 0
    flagged_students = Dict()
    for idx_student = 2:num_students+1
        answer_vector = data_cleaned[idx_student,3:end]

        answer_vector_size = size(answer_vector,1)

        if !checkEachAnswerEngine(answer_vector, data_key_size)
            flagged_students[idx_student] = [data_cleaned[idx_student,1], data_cleaned[idx_student,2]]
        end

        if !checkAnswerEndSpaceEngine(answer_vector, data_key_size)
            flagged_students[idx_student] = [data_cleaned[idx_student,1], data_cleaned[idx_student,2]]
        end

    end

    return flagged_students
end
