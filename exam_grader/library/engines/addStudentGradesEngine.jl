# Author: Jaime Lopez-Merizalde
# Date: 26 10 2020
# Last Update: 27 10 2020
# Title: addStudentGradesEngine.jl
# Descripition:
#  add loaded raw grades to an existing data structure of student grades
# REPO:
#NOTES:written in Julia 0.5.4


#---- Library
include("$(pwd())/library/engines/names_splitting_engine.jl")
include("$(pwd())/library/engines/subStringToString.jl")
include("$(pwd())/library/engines/name_search_engine.jl")


function addStudentGradesEngine(data_raw_canvas, data_graded_midterm, midterm_name, exam_points; where_to_add = "Assignments Current Points")

    #data fields
    data_fields = data_raw_canvas[1,:]

    #student names
    student_names = data_raw_canvas[3:end,1]
    num_students = size(student_names,1)
    where_to_add_column = find(x->x==where_to_add, data_fields)[1]-1

    #check if the column exists already before expanding
    data_raw_canvas_w_midterm = data_raw_canvas
    #this will not check if the midterm has a unique identifier attached to its name once it is downloaded
    #this happens in canvas, so be careful if the column already exists in the database
    if length(find(x->x==midterm_name, data_fields)) > 0
        println("FIELD NAME ALREADY EXISTS! COLUMN: $(find(x->x==midterm_name, data_fields)[1]) ")
        #set expanded raw canvas data exactly as the canvas data
        # data_raw_canvas_w_midterm = data_raw_canvas
        #define where to add column as the first place where a column match exists
        where_to_add_column = find(x->x==midterm_name, data_fields)[1]-1
        else
        data_raw_canvas_w_midterm = expandDataRawCanvas(data_raw_canvas, where_to_add_column, midterm_name, exam_points)
        end

    #transform data type of graded midterm data first and last names
    for idx_data_midterm = 1:size(data_graded_midterm,1)
        data_graded_midterm[idx_data_midterm,1] = subStringToString(data_graded_midterm[idx_data_midterm,1])
        data_graded_midterm[idx_data_midterm,2] = subStringToString(data_graded_midterm[idx_data_midterm,2])
        end

    ##Split student names
    student_names_extracted_from_raw_data = namesSplittingEngine(student_names; header = false, rows = 0)

    ##add the student grade
    for idx_student = 1:num_students

        #idx_student: index on data_raw_canvas with names only. last, first

        which_student = [student_names_extracted_from_raw_data[idx_student,1], student_names_extracted_from_raw_data[idx_student,2]]

        data_raw_canvas_w_midterm = addSingleStudentGrade(which_student, idx_student, data_graded_midterm, where_to_add_column, data_raw_canvas_w_midterm)

        end

    return data_raw_canvas_w_midterm

    end

function addSingleStudentGrade(which_student, idx_student, data_graded_midterm, where_to_add_column, data_raw_canvas_w_midterm)

    #must match the case, so we just ignore it entirely
    last_name_Xcheck = uppercase(which_student[1])
    first_name_Xcheck = uppercase(which_student[2])

    bool_name_found, idx_student_found = nameSearchEngine(last_name_Xcheck, first_name_Xcheck, data_graded_midterm)

    #add the grade to the student in
    if bool_name_found
        data_raw_canvas_w_midterm[idx_student+2, where_to_add_column+1] = data_graded_midterm[idx_student_found,4][1]
        else
            println("DID NOT FIND STUDENT: $(last_name_Xcheck), $first_name_Xcheck IN THE GRADED MIDTERM LIST. SETTING GRADE TO 0 \n")
            data_raw_canvas_w_midterm[idx_student+2, where_to_add_column+1] = 0
        end

    return data_raw_canvas_w_midterm

    end

function expandDataRawCanvas(data_raw_canvas, where_to_add_column, midterm_name, exam_points)

    data_raw_canvas_size = size(data_raw_canvas)

    data_fields = data_raw_canvas[1,:]

    #Add column to the new csv data

    #create new array w one more column
    data_raw_canvas_w_midterm = Array{Any}(data_raw_canvas_size[1], data_raw_canvas_size[2]+1)

    #pre-populate with existing columns
    data_raw_canvas_w_midterm[:,1:where_to_add_column] = data_raw_canvas[:,1:where_to_add_column]
    data_raw_canvas_w_midterm[:,where_to_add_column+2:end] = data_raw_canvas[:,where_to_add_column+1:end]

    #add label and points to new column:
    #label and points for the new column
    data_raw_canvas_w_midterm[1, where_to_add_column+1] = midterm_name
    data_raw_canvas_w_midterm[2, where_to_add_column+1] = exam_points

    return data_raw_canvas_w_midterm

    end
