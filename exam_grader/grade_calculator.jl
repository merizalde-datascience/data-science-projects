# Author: Jaime Lopez-Merizalde
# Date: 09 10 2020
# Last Update: 28 11 2020
# Title: grade_calculator
# Descripition:
#  Calculate Grade and save to a txt file
# this code written in Julia 0.5.4
# REPO:
#NOTES:written for Julia 0.5.4
#
#28 10 2020: if any mistakes in problems were made, we have to have a way to comp
#the issue and give a free credit regardless of answer, my idea is to give the key a
#wild card character and have the software recognize this and ignore the student input

##-----Libraries
include("$(pwd())/library/engines/subStringToString.jl")

# using DelimitedFiles

##DATA
##-----DATA
#-----data file names
#
data_midterm_FN_pre = "midterm_data_3_cleaned.txt"
data_midterm_FN = "$(pwd())/$data_midterm_FN_pre"

data_key_FN_pre = "key_midterm_3.txt"
data_key_FN = "$(pwd())/$data_key_FN_pre"

save_delimited_file_FN_pre = "GRADED_MIDTERM_3_RAW.txt"
save_delimited_file_FN = "$(pwd())/$save_delimited_file_FN_pre"

save_delimited_file_FN_pre_formatted = "GRADED_MIDTERM_3_FORMATTED.txt"
save_delimited_file_FN = "$(pwd())/$save_delimited_file_FN_pre_formatted"

#-----data loading
data_midterm = readdlm(data_midterm_FN,',')
data_midterm_size = size(data_midterm,1)

data_fields = data_midterm[1,:]

data_key = readdlm(data_key_FN,',')
data_key_size = size(data_key,1)

#-------Fields

#count the students
#copunt how many students were entered and decide if this is the appropriate amount
num_students = data_midterm_size-1

#how many responses?
num_questions = size(data_key[2,:],1)

const points_per_question = 10
total_points = points_per_question*num_questions

#-----Grade student engines
function questionGraderEngine(student_data, key)
        #student_data: last, first, vector of responses

        #grade each problem, find the total correct, set the array from that
        correct_question_counter = 0
        num_questions = size(key,1)

        for idx_questions = 1:num_questions
                #'*': is a wildcard character, it's my way of accounting for an incorrect problem and giving the student free credit for the problem no matter what they put down

                bool_problem_correct = (student_data[idx_questions+2]==key[idx_questions] || subStringToString(key[idx_questions])==" *")

                if bool_problem_correct
                    correct_question_counter+=1
                end
        end
        student_data[3]=correct_question_counter

        #define the student score
        student_data[4] = student_data[3]*points_per_question

        #define the percent score
        student_data[5] = round(student_data[4]/total_points*100)

        return student_data[3:5]
end

function letterScoreEngine(student_percent_grade)
    letter_grade = string()
    percent_array = [93, 90, 87, 83, 80, 77, 73, 70, 67, 63, 60, 0]
    grade_letter_array = ["A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"]

    percent_array_iterator = 1
    while student_percent_grade < percent_array[percent_array_iterator]
        percent_array_iterator+=1
    end
    letter_grade = grade_letter_array[percent_array_iterator]

    return letter_grade
end

function createCharacterCountArray(field_array)
    field_array_size = size(field_array,1)

    #for each element in the array populate a new array with letter counts
    character_count_array = Array{Any}(field_array_size,2)
    character_count_array[:,1] = field_array
    for idx_fields = 1:field_array_size
        character_count_array[idx_fields,2]=length(field_array[idx_fields])
    end

    return character_count_array
    end

function formatArrayForDelimStyle(unformatted_array)

    unformatted_array_size = size(unformatted_array)

    formatted_array = Array{Any}(unformatted_array_size)

    character_count_array = createCharacterCountArray(unformatted_array[1,:])

    #take the fields row
    formatted_array[1,:] = unformatted_array[1,:]

    #check each element in the unformatted array
    for idx_data_rows = 2:unformatted_array_size[1]
        for idx_data_columns = 1:unformatted_array_size[2]
            unformatted_array_element = unformatted_array[idx_data_rows,idx_data_columns]

            character_count_array_element = character_count_array[idx_data_columns,2]

            unformatted_array_element_type = typeof(unformatted_array_element)

            unformatted_array_type = typeof(unformatted_array[idx_data_rows,idx_data_columns])

            #check data type first
            #work on strings first
            if unformatted_array_element_type==String || unformatted_array_element_type==SubString{String}

                unformatted_array_element_count = length(unformatted_array_element)
                #element size too large
                if (unformatted_array_element_count>character_count_array_element)

                    #if the character count in unformatted array is LARGER then downsize for the formatted array
                    formatted_array[idx_data_rows,idx_data_columns] = string(unformatted_array_element[1:character_count_array_element])

                #same size
                elseif unformatted_array_element_count==character_count_array_element
                    formatted_array[idx_data_rows,idx_data_columns]=string(unformatted_array_element)

                #element size too small
                elseif (unformatted_array_element_count<character_count_array_element)

                    unformatted_array_element_count = length(unformatted_array_element)
                    char_deficiency = character_count_array_element - unformatted_array_element_count

                    #add spaces to make up for the deficiency
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element
                    for idx_spaces = 1:char_deficiency
                        formatted_array[idx_data_rows,idx_data_columns]=string(formatted_array[idx_data_rows,idx_data_columns]," ")
                    end
                end

            #next work on the actual numberical values
            #first change the number to a string
            elseif unformatted_array_element_type==Int64 || unformatted_array_element_type== Float64

                #change numericals to strings
                unformatted_array_element_as_string = string(unformatted_array_element)
                unformatted_array_element_count = length(unformatted_array_element_as_string)

                #element size too large
                if (unformatted_array_element_count>character_count_array_element)

                    #if the character count in unformatted array is LARGER then downsize for the formatted array
                    formatted_array[idx_data_rows,idx_data_columns] = unformatted_array_element_as_string[1:character_count_array_element]

                #same size
                elseif unformatted_array_element_count==character_count_array_element
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element_as_string

                #element size too small
                elseif (unformatted_array_element_count<character_count_array_element)

                    char_deficiency = character_count_array_element - unformatted_array_element_count

                    #add spaces to make up for the deficiency
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element_as_string
                    for idx_spaces = 1:char_deficiency
                        formatted_array[idx_data_rows,idx_data_columns]=string(formatted_array[idx_data_rows,idx_data_columns]," ")
                    end
                end
            end

        end
    end
    return formatted_array, character_count_array
end

#-------grade the students responses
student_grade_array = Array{Any,2}(num_students+1,6)
#six fields: last name, first name, correct problems, points earned, percent score, letter grade
student_grade_array[1,:] = ["LAST NAME ", "FIRST NAME ", "NO. CORRECT ", "POINTS EARNED ", "PERCENT SCORE ", "LETTER GRADE "]


for idx_student = 2:num_students+1
    #populate the name fields
    student_grade_array[idx_student,1:2] = data_midterm[idx_student,1:2]

    #get the questions graded
    student_grade_array[idx_student,3:5] = questionGraderEngine(data_midterm[idx_student,:], data_key[2,:])

    #define the letter score
    student_grade_array[idx_student,6] = letterScoreEngine(student_grade_array[idx_student,5])
end

#Format the graded array so it's easy to read when it's saved as .txt
student_grade_array_formatted, character_count_array = formatArrayForDelimStyle(student_grade_array)
#six fields: last name, first name, correct problems, points earned, percent score, letter grade


#-----TO DISPLAY

#-----TO SAVE
#-----Save raw student data to a .txt or csv file
open("$save_delimited_file_FN_pre", "w") do io
    writedlm(io, student_grade_array,',')
end

#-----Save formatted student data to a .txt or csv file
open("$save_delimited_file_FN_pre_formatted", "w") do io
    writedlm(io, student_grade_array_formatted,',')
end
