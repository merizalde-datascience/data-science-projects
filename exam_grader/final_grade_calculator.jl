# Author: Jaime Lopez-Merizalde
# Date: 09 10 2020
# Last Update: 2 12 2020
# Title: final_grade_calculator
# Descripition:
#  Calculate Grade and save to a txt file
# this code written in Julia 0.5.4
# REPO:
#NOTES:written for Julia 0.5.4
#

##-----Libraries
include("$(pwd())/library/engines/subStringToString.jl")

# using DelimitedFiles

##DATA
##-----DATA
#-----data file names
#
data_midterm_FN_pre = "2020-12-02T0922_Grades-MATH-1230-05Fa20.csv"
data_midterm_FN = "$(pwd())/../$data_midterm_FN_pre"

save_delimited_file_FN_pre = "FINAL_GRADE_RAW.txt"
save_delimited_file_FN = "$(pwd())/../final grade calculations/$save_delimited_file_FN_pre"

save_delimited_file_FN_pre_formatted = "FINAL_GRADE_RAW_FORMATTED.txt"
save_delimited_file_FN_formatted = "$(pwd())/../final grade calculations/$save_delimited_file_FN_pre_formatted"

#-----data loading
data_midterm = readdlm("$data_midterm_FN",',')
data_midterm_size = size(data_midterm,1)

data_fields = data_midterm[1,:]


#-------Fields

#count the students
#copunt how many students were entered and decide if this is the appropriate amount

#-----Grade student engines
function finalLetterGradeEngine(data_midterm)

    augmented_data_midterm = Array{Any}(size(data_midterm,1),size(data_midterm,1)+1)
    finalColumnName = "Letter Grade"
    augmented_data_midterm[1, end] = finalColumnName
    augmented_data_midterm[2, end] = "(read only)"

    num_students = size(data_midterm,1) - 2
    #student_data: last, first, vector of responses

    for idx_students = 1:num_students
            augmented_data_midterm[idx_students,end] = letterScoreEngine(data_midterm[idx_students,end])
    end

    return augmented_data_midterm
    end

function letterScoreEngine(student_percent_grade)
    letter_grade = string()
    percent_array = [93, 90, 87, 83, 80, 77, 73, 70, 67, 63, 60, 0]
    grade_letter_array = ["A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"]

    percent_array_iterator = 1
    while student_percent_grade < percent_array[percent_array_iterator]
        percent_array_iterator+=1
    end
    letter_grade = grade_letter_array[percent_array_iterator]

    return letter_grade
    end

function createCharacterCountArray(field_array)
    field_array_size = size(field_array,1)

    #for each element in the array populate a new array with letter counts
    character_count_array = Array{Any}(field_array_size,2)
    character_count_array[:,1] = field_array
    for idx_fields = 1:field_array_size
        character_count_array[idx_fields,2]=length(field_array[idx_fields])
    end

    return character_count_array
    end

function formatArrayForDelimStyle(unformatted_array)

    unformatted_array_size = size(unformatted_array)

    formatted_array = Array{Any}(unformatted_array_size)

    character_count_array = createCharacterCountArray(unformatted_array[1,:])

    #take the fields row
    formatted_array[1,:] = unformatted_array[1,:]

    #check each element in the unformatted array
    for idx_data_rows = 2:unformatted_array_size[1]
        for idx_data_columns = 1:unformatted_array_size[2]
            unformatted_array_element = unformatted_array[idx_data_rows,idx_data_columns]

            character_count_array_element = character_count_array[idx_data_columns,2]

            unformatted_array_element_type = typeof(unformatted_array_element)

            unformatted_array_type = typeof(unformatted_array[idx_data_rows,idx_data_columns])

            #check data type first
            #work on strings first
            if unformatted_array_element_type==String || unformatted_array_element_type==SubString{String}

                unformatted_array_element_count = length(unformatted_array_element)
                #element size too large
                if (unformatted_array_element_count>character_count_array_element)

                    #if the character count in unformatted array is LARGER then downsize for the formatted array
                    formatted_array[idx_data_rows,idx_data_columns] = string(unformatted_array_element[1:character_count_array_element])

                #same size
                elseif unformatted_array_element_count==character_count_array_element
                    formatted_array[idx_data_rows,idx_data_columns]=string(unformatted_array_element)

                #element size too small
                elseif (unformatted_array_element_count<character_count_array_element)

                    unformatted_array_element_count = length(unformatted_array_element)
                    char_deficiency = character_count_array_element - unformatted_array_element_count

                    #add spaces to make up for the deficiency
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element
                    for idx_spaces = 1:char_deficiency
                        formatted_array[idx_data_rows,idx_data_columns]=string(formatted_array[idx_data_rows,idx_data_columns]," ")
                    end
                end

            #next work on the actual numberical values
            #first change the number to a string
            elseif unformatted_array_element_type==Int64 || unformatted_array_element_type== Float64

                #change numericals to strings
                unformatted_array_element_as_string = string(unformatted_array_element)
                unformatted_array_element_count = length(unformatted_array_element_as_string)

                #element size too large
                if (unformatted_array_element_count>character_count_array_element)

                    #if the character count in unformatted array is LARGER then downsize for the formatted array
                    formatted_array[idx_data_rows,idx_data_columns] = unformatted_array_element_as_string[1:character_count_array_element]

                #same size
                elseif unformatted_array_element_count==character_count_array_element
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element_as_string

                #element size too small
                elseif (unformatted_array_element_count<character_count_array_element)

                    char_deficiency = character_count_array_element - unformatted_array_element_count

                    #add spaces to make up for the deficiency
                    formatted_array[idx_data_rows,idx_data_columns]=unformatted_array_element_as_string
                    for idx_spaces = 1:char_deficiency
                        formatted_array[idx_data_rows,idx_data_columns]=string(formatted_array[idx_data_rows,idx_data_columns]," ")
                    end
                end
            end

        end
    end
    return formatted_array, character_count_array
    end

#-------grade the students responses
num_students = size(data_midterm,1) - 2
student_grade_array = Array{Any,2}(num_students,3)
#six fields: last name, first name, correct problems, points earned, percent score, letter grade
student_grade_array[1,:] = ["NAME LAST FIRST ", "PERCENT GRADE ", "LETTER GRADE "]

for idx_student = 2:num_students
    #populate the name fields
    student_grade_array[idx_student,1] = data_midterm[idx_student+1,1]

    #populate percent score
    student_grade_array[idx_student,2] = data_midterm[idx_student+1,end]

    #define the letter score
    student_grade_array[idx_student,3] = letterScoreEngine(data_midterm[idx_student+1,end])
    end

#Format the graded array so it's easy to read when it's saved as .txt
student_grade_array_formatted, character_count_array = formatArrayForDelimStyle(student_grade_array)
#six fields: last name, first name, correct problems, points earned, percent score, letter grade

#Mean of Grades
x_bar_grades = mean(student_grade_array[2:end,2])
x_variance_grades = var(convert(Array{Float64,1},student_grade_array[2:end,2]));


#-----TO DISPLAY
println("mean of class grades: $x_bar_grades = $(letterScoreEngine(x_bar_grades))")
println("variance of class grades: $x_variance_grades")

#-----TO SAVE
#-----Save raw student data to a .txt or csv file
open("$save_delimited_file_FN", "w") do io
    writedlm(io, student_grade_array,',')
end

#-----Save formatted student data to a .txt or csv file
open("$save_delimited_file_FN_formatted", "w") do io
    writedlm(io, student_grade_array_formatted,',')
end
