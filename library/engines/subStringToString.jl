# Author: Jaime Lopez-Merizalde
# Date: 13 10 2020
# Last Update: 13 10 2020
# Title: subStringToString.jl
# Descripition:
#  take a substring, return the string only.
# REPO:
#NOTES:written in Julia 0.5.4

function subStringToString(SubString::String)
    #don't do anything if it is a string!
    return SubString
end

function subStringToString(SubString::SubString{String})

    #takes a Substring returns as a string
    substring_length = length(SubString)

    string_to_return = ""
    for idx_char = 1:substring_length
        string_to_return = string(string_to_return, SubString[idx_char])
    end

    return string_to_return
end

function subStringToString(array::Array{Any,2})

    #an array of substrings
    num_array_elements = size(array)

    Array_as_string = Array{String}(num_array_elements[1],num_array_elements[2])

    for idx_array_element = 1:num_array_elements[1]

        Array_as_string[idx_array_element,1] = subStringToString(array[idx_array_element,1])

        Array_as_string[idx_array_element,2] = subStringToString(array[idx_array_element,2])

    end

    return Array_as_string
end
