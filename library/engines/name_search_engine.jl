# Author: Jaime Lopez-Merizalde
# Date: 13 10 2020
# Last Update: 13 10 2020
# Title: name_search_engine.jl
# Descripition:
#  find names in an array with last and first names populating
# REPO:
#NOTES:written in Julia 0.5.4


#---- Library
include("$(pwd())/library/engines/subStringToString.jl")

function nameSearchEngine(last_name_Xcheck, first_name_Xcheck, list_of_names)

    #list_of_names: col1 = last, col2 = first, no header

    last_name_Xcheck = uppercase(last_name_Xcheck)
    first_name_Xcheck = uppercase(first_name_Xcheck)

    #try to find the name in the data to clean list, by last then by first
    bool_name_found = false

    list_of_names = subStringToString(list_of_names)

    idx_last_name_search = find(x->x==last_name_Xcheck,list_of_names[:,1])

    idx_to_return = 0

    # if you found it
    ## double check the first name matches
    if size(idx_last_name_search,1) == 1

        #don't check the first name if the last name is an immediate match
        # bool_name_found = (first_name_Xcheck == list_of_names[idx_last_name_search,2][1])

        bool_name_found = true
        if bool_name_found
            idx_to_return = idx_last_name_search
        end

    elseif size(idx_last_name_search,1) > 1

        #find the array with
        reduced_list_of_names = list_of_names[:,2][idx_last_name_search]


        idx_which_name_reduced = find(x->x==first_name_Xcheck, reduced_list_of_names)

        #after multiple last name matches, you successfully found the unique first name match as well:
        if size(idx_which_name_reduced,1) == 1

            idx_unique_last_first_name = idx_last_name_search[idx_which_name_reduced]

            idx_to_return = idx_unique_last_first_name
            bool_name_found = true
        end
    end

    return bool_name_found, idx_to_return

end
