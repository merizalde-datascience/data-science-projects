# Author: Jaime Lopez-Merizalde
# Date: 13 10 2020
# Last Update: 13 10 2020
# Title: names_splitting_engine.jl
# Descripition:
#  split names from array concatenated into one cell differentiated by ','
# REPO:
#NOTES:written in Julia 0.5.4
#

function extractNamesEngine(names_splitting_array, names_array)
    #this does not account for headers, only a raw list of names
    num_names = size(names_splitting_array,1)

    names_extracted = Array{String}(num_names,2)

    #based on what happend in the splitting engine, reassign the names

    for idx_names = 1:num_names
        #tells me where the split point is for this name
        split_point = names_splitting_array[idx_names]

        #last name
        names_extracted[idx_names,1] = names_array[idx_names][1:split_point-1]

        #first name
        #do include the space so it matches the other records
        names_extracted[idx_names,2] = names_array[idx_names][split_point+1:end]
    end
    ##_EXTRACTED STUDENT NAMES  ENGINE

    return names_extracted
end

##_STUDENT NAMES SPLITTING ENGINE
##: for each row entry, the point where last and first are separated is by a , then follows a space
##: student names character length split array
function namesSplittingEngine(listOfNames; header = false, rows = 0)

    num_names = size(listOfNames,1)
    if header
        header_correction = rows
    else
        header_correction = 0
    end

    ##this array tells me where the name cutoff is from last to first
    names_splitting_array = Array{Int64}(num_names)
    for idx_names = header_correction+1:num_names

        name = listOfNames[idx_names]

        char_search_stop = false
        char_iterator = 1
        while !char_search_stop

            #check the character
            if name[char_iterator] != ','
                char_iterator+= 1
            else
                char_search_stop = true
                names_splitting_array[idx_names] = char_iterator
            end

            #don't check too many characters
            if char_iterator >= 25
                char_search_stop = true

                #just set this to the current character count
                names_splitting_array[idx_names] = char_iterator
            end
        end


    end

    names_array = listOfNames[header_correction+1:end]

    return extractNamesEngine(names_splitting_array, names_array)

end
