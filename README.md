Author: Jaime Lopez-Merizalde
Programs: Julia 0.5.4
Contact: email: jaime.meriz13@gmail.com

Description:

This repo is a personal data science repo made publically available to demonstrate the author's coding and development abilities.
These are exemplified through the various project folders.


About the Author:
The author was born in Quito, Ecuador in 1988 before moving to Nebraska in 1997. He graduated from the University of Nebraska with dual degrees in Civil Engineering and Architectural Engineering in 2012. The author started a PhD program in the Tulane University Department of Mathematics in August of 2013. While earning his doctorate degree, he had the wondeful opportunity of interning at Los Alamos National Laboratory and General Electric Co. He earned his Ph.D. in May of 2020.

